#!/usr/bin/python
from datetime import datetime
from dhcpmonitor import DHCPMonitor
from light import Light
import ConfigParser

def basicprint(hwaddr, event, info, extra):
    name = hwaddr
    if (info.has_key('dhcpClientID') and info['dhcpClientID'] != ''):
        name = info['dhcpClientID']
    if (event == 'incoming'):
        print "[%s] %s has %s" % (datetime.now(), name, 'arrived')
    elif (event == 'outgoing'):
        print "[%s] %s has %s" % (datetime.now(), name, 'departed')

if __name__ == "__main__":
    d = DHCPMonitor(ip='10.0.1.1')
    d.register('*','incoming', basicprint)
    d.register('*','outgoing', basicprint)
    #run
    d.run()
